;;; All Rights Reserved (C) Jacob Hrbek <rsa4096/0x31AE4020956E0A9A> in 29/10/2021-EU 21:27:26 UTC
;;; TODO(Krey): Release under FLOSS once finished

;;; This file is the gateway in the system managing solution

(use-modules (gnu))

;; FIXME(Krey): Process DOMAIN

;; function to throw error if environment variable 'HOSTNAME' is not set
 ; or if it's value is empty
;; FIXME-QA(Molese): Needs test
(define system-hostname-check (lambda ()
    (if (eq nil (getenv "HOSTNAME"))
        (throw 1 "Environment variable 'HOSTNAME' is not set, unable to apply sufficient configuration"))
    (if (eq? (getenv "HOSTNAME") #f)
        (throw 1 "Environment variable 'HOSTNAME' is empty, unable to apply sufficient configuration"))))

(define system-hostname (getenv "HOSTNAME"))

;; TODO(Krey): Process https://guix.gnu.org/manual/en/html_node/Invoking-guix-deploy.html

(load "users/kreyren.scm")
(load "domains/rixotstudio.cz/default.scm")

(display "Finished")(newline)
