#!/usr/bin/env nix-shell
# NixOS shell configuration to bootstrap the required dependencies to work with the source code

{
	nixpkgs ? builtins.fetchGit {
		url = "ssh://git@github.com:NixOS/nixpkgs.git";
		ref = "nixos-21.05";
		rev = "66d6ec6ed2ddc77d08cf677cc8230db39f051793"; # 29/10/2021-EU 21:21:40 CEST
}:
with nixpkgs;
	stdenv.mkDerivation {
		name = "RiXotStudio";
		buildInputs = [
			cargo-make # Used to interpret `Makefile.toml` to interact with the repository
			hadolint # Used to lint dockerfiles
			docker # Used to run the dockerfiles
		];
	}
