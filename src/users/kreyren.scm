;;;@ All Rights Reserved (C) Jacob Hrbek <https://keys.openpgp.org/search?q=kreyren%40rixotstudio.cz> in 29/10/2021-EU 21:27:26 UTC

;; This file is expected to be loaded e.g. `(load path/to/file)` in the file passed to `guix system reconfigure` to configure the user account. It is important that this file is kept independent from the implementation

;;; # Kreyren
;;; The user account of RiXotStudio's Headmaster

;;; REPRODUCIBILITY CHECKLIST
;;;	- [ ] IceCat
;;;		- [ ] Extensions
;;;			- [ ] DarkReader
;;;			- [ ] Ublock origin
;;;			- [ ] Decentraleyes
;;;			- [ ] Privacy Redirect
;;;			- [ ] LocalCND
;;;			- [ ] SponsorBlock
;;;			- [ ] Don't track me Google
;;;			- [ ] I don't care about cookies
;;;			- [ ] ClearURLs
;;;			- [ ] NoScript
;;;			- [ ] Forget Me Not
;;;			- [ ] Metastream Remote
;;;			- [ ] Read Aloud
;;;			- [ ] KeepAssXC
;;;			- [ ] Video Speed Controller
;;;		- [ ] Configuration
;;;			- [ ] Theme: Dark
;;;			- [ ] Privacy & Security > Delete cookies and site data when IceCat is closed
;;;			- [ ] Privacy & Security > History > Always use private browsing mode
;;;			- [ ] Set SOCKS5 proxy on 9050 with deployed tor
;;;	- [ ] Alacritty
;;;	- [ ] XFCE4
;;; 		- [ ] Wallpaper: Solid Black Color
;;;		- [ ] Window Manager style: Daloa
;;;		- [ ] Focus follows mouse
;;; 		- [ ] Keybinds
;;;			- [ ] Super_L + Enter = exo-open --launch TerminalEmulator
;;;			- [ ] Super_L + Key_T = exo-open --launch WebBrowser
;;;			- [ ] Super_L + Key_H = Hide focused window
;;;			- [ ] Super_L + Key_D = dmenu_run
;;;			- [ ] Super_L + Key_L = xflock4
;;;			- [ ] Alt_L + Shift_L + PrintScreen = flameshot gui
;;;			- [ ] Audio mute = pactl set-sink-mute @DEFAULT_SINK@ toggle
;;;			- [ ] Audio raise volume = pactl set-sink-volume @DEFAULT_SINK@ +2.5%
;;;			- [ ] Audio lower volume = pactl set-sink-volume @DEFAULT_SINK@ -2.5%
;;;	- [ ] KeepAssXC
;;;		- [ ] Configured to open Kreyren.kdbx by default
;;;		- [ ] Enable browser integration
;;;	- [ ] Conky
;;;		- [ ] Configure
;;;	- [ ] dmenu
;;;	- [ ] Nextcloud
;;; 		- [ ] Credentials pre-set to open on user login and start sync
;;;	- [ ] Flameshot
;;;		- [ ] Sync all required dirs
;;;	- [ ] Pulseaudio
;;;		- [ ] Set 'load-module module-echo-cancel' to do active noise filtering
;;;	- [ ] Vim
;;;	- [ ] Tor
;;;		- [ ] TorSOCKS
;;;	- [ ] Matrix client
;;;		- [ ] element-desktop?
;;;	- [ ] htop
;;;	- [ ] xclip
;;;	- [ ] GnuPG
;;;		- [ ] pinentry-tty
;;;	- [ ] vlc
;;;		- [ ] Configuration
;;;			- [ ] Use tor for remotes
;;;	- [ ] qbittorrent
;;;		- [ ] Get search engines

;; When this file is `(load)`-ed set user kreyren
(operating-system (users (cons* (user-account
	(name "kreyren")
	(comment "Jacob Hrbek")
	(group "users")
	(shell (file-append bash "/bin/bash"))
	;; DND(Krey): Figure out how to do the password
	;(password (crypt "InitialPassword!" "$6$abc"))
	(supplementary-groups '(
		"wheel"
		"netdev"
		"audio"
		"video"))))))

;; DND(Krey): Handle SSH access
