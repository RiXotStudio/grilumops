;;; # Minimal GUIX
;;; configuration used for development and testing

(operating-system
	(host-name "guix")
	(timezone "Etc/UTC")
	(locale "en_US.utf8")

	(keyboard-layout (keyboard-layout "us" "altgr-intl"))
	(bootloader (bootloader-configuration
		(bootloader grub-bootloader)
		(target "/dev/sda")
		(timeout 10))

	(file-systems (append
		(list (file-system
			(mount-point "/")
			(device (file-system-label "GUIX"))
			(type "btrfs"))
		%base-file-systems))

	(users (cons
		(user-account
			(name "guix")
			(group "users")
			(password "guix")
			(supplementary-groups '(
				"wheel")))
	%base-user-accounts))

	(packages (append (list
		nss-certs)
	%base-packages))

	(services (append (list
		(service openssh-service-type))
	%desktop-services))
