#!/usr/bin/env -S emacs --no-splash -q -l
;;; Initialization file for emacs designed to provide the standard environment to work with the project

;; NOT_FINISHED(Krey): Work in progress

;; We assume that all required packages are already installed in emacs which is expected to be handled through cargo-make's Makefile.toml

;; Q(Krey): Should we ask the end-user whether they want to use evil?
(evil-mode 1)

;;; KEYBINDS
;; Treemacs
(define-key global-map (kbd "C-x C-\\") 'treemacs)


;;; EXPERIMENT
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(doom-outrun-electric)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(message "hello")
