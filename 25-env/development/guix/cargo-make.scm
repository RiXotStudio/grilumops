;;; GNU Guix --- Functional package management for GNU
;;; Copyright (C) 2021 Jacob Hrbek <rsa4096/0x3E0E1A60C82873AD>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu packages cargo-make)
	       #:use-module (guix packages)
	       #:use-module (guix download)
	       #:use-module (guix build-system cargo)
	       #:use-module ((guix licenses) #:prefix license:))

(define-public rust-cargo-make-0.35
  (package
    (name "rust-cargo-make")
    (version "0.35.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cargo-make" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vi0i7l01dck1swzjrj0z9033nnszisr34xdw94vnbmjjy6yvg71"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-ansi-term" ,rust-ansi-term-0.12)
         ("rust-cargo-metadata" ,rust-cargo-metadata-0.14)
         ("rust-ci-info" ,rust-ci-info-0.14)
         ("rust-clap" ,rust-clap-2)
         ("rust-colored" ,rust-colored-2)
         ("rust-dirs-next" ,rust-dirs-next-2)
         ("rust-duckscript" ,rust-duckscript-0.7)
         ("rust-duckscriptsdk" ,rust-duckscriptsdk-0.8)
         ("rust-envmnt" ,rust-envmnt-0.9)
         ("rust-fern" ,rust-fern-0.6)
         ("rust-fsio" ,rust-fsio-0.3)
         ("rust-git-info" ,rust-git-info-0.1)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-home" ,rust-home-0.5)
         ("rust-ignore" ,rust-ignore-0.4)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-regex" ,rust-regex-1)
         ("rust-run-script" ,rust-run-script-0.9)
         ("rust-rust-info" ,rust-rust-info-0.3)
         ("rust-semver" ,rust-semver-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-ignored" ,rust-serde-ignored-0.1)
         ("rust-shell2batch" ,rust-shell2batch-0.4)
         ("rust-toml" ,rust-toml-0.5))))
    (home-page "https://sagiegurari.github.io/cargo-make")
    (synopsis "Rust task runner and build tool.")
    (description "Rust task runner and build tool.")
    (license license:asl1.0)))

