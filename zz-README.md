# Grilumops

Fork adaptation of RiXotStudio's Nixumops into a Guile scheme used in e.g. Guix GNU/Linux

### Origin of the name

Grile was a typo by @kreyren when converting the name from Nixumops to guile scheme

### Credit
CRE1. Inspired by Julien Lepiller's configuration in https://framagit.org/tyreunom/system-configuration
