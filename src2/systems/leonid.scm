;;; All Rights Reserved (C) Jacob Hrbek <rsa4096/0x31AE4020956E0A9A> in 29/10/2021-EU 21:27:26 UTC
;;; TODO(Krey): Release under FLOSS once finished

;;; This file defines a configuration for the Leonid system
;;; Expected:
;;; - [ ] XFCE4
;;; - [ ] SSH
;;; - [ ] SSH+TOR
;;; - [ ] Bind9
;;; - [ ] Docker
;;; - [ ] Tor Private Bridge
;;; - [ ] Xen

(use-service-modules
	cups
	desktop
	networking
	ssh
	xorg)

(operating-system
	(host-name "leonid")
	(timezone "Europe/Prague")
	(locale "en_US.utf8")
	(keyboard-layout (keyboard-layout "us"))

	(bootloader (bootloader-configuration
		(bootloader grub-bootloader)
		(targets "/dev/sda")
		;; SECURITY(Krey): Our bootloader is encrypted so we can allow timeout higher than 0
		(timeout 5)
		(keyboard-layout keyboard-layout)))

	(mapped-devices
		(list (mapped-device
			(source (uuid "1fe117ac-fe47-4eb3-a63b-b8a34dee4ea8"))
			(target "cryptroot")
			(type luks-device-mapping))))

	(file-systems (cons* (file-system
			(mount-point "/")
			(device "/dev/mapper/cryptroot")
			(type "btrfs")
			(dependencies mapped-devices))
		%base-file-systems))

	 (users (append my-users %base-users))

	(packages
	 (append
	  (list (specification->package "nss-certs"))
	  %base-packages))

        (services
	 (append
	  (list (service xfce-desktop-service-type)
		(service openssh-service-type)
		(service tor-service-type)
		(set-xorg-configuration
		 (xorg-configuration
                  (keyboard-layout keyboard-layout))))
	  %desktop-services)))
