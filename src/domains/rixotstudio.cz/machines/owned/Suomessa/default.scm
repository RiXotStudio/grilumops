;;; This file defines a configuration for the Dreamon system
;;; Expected:
;;; - [ ] Submit Guile API to Hetzner <https://github.com/hetznercloud/awesome-hcloud>
;;;     - [ ] A. Integrate the API
;;;     - [ ] B. Set up the server manually like a pleb
;;; - [ ] SSH
;;; - [ ] SSH+TOR
;;; - [ ] Bind9
;;;     - [ ] Authoritative DNS for RiXotStudio.cz domain
;;; - [ ] Tor Relay
;;; - [ ] Fira fonts
;;; - [ ] Git Repository Frontend
;;;     - [ ] Gitea?
;;;     - [ ] cgit?
;;;     - [ ] Smithy <https://git.pokorny.ca/smithy>?
;;;     - [ ] Define our own?
;;; - [ ] PrivateBin
;;; - [ ] Mastodon
;;; - [ ] Matrix Homeserver
;;;     - [ ] Decide on the homeserver
;;; - [ ] Discourse
;;; - [ ] NextCloud
;;; - [ ] Invidious
;;; - [ ] Etherdoc
;;; - [ ] Hedgedoc
;;; - [ ] NextCloud
;;; - [ ] SearX
;;; - [ ] Jisi
;;; - [ ] Nitter
;;; - [ ] Bibliogram