;;; This file defines a configuration for the Dreamon system
;;; Expected:
;;; - [ ] XFCE4
;;; - [ ] SSH
;;; - [ ] SSH+TOR
;;; - [ ] Bind9
;;; - [ ] Docker
;;; - [ ] Tor Private Bridge
;;; - [ ] Xen
;;; - [ ] Fira fonts
;;; - [ ] Set `load-module module-echo-cancel` for pulseaudio