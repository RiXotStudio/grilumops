;;; This file defines a configuration for the Schutr64 system
;;; Expected:
;;; - [ ] SSH
;;; - [ ] SSH+TOR
;;; - [ ] Tor Private Bridge
;;; - [ ] Fira fonts