;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules
  cups
  desktop
  networking
  ssh
  xorg)

(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Prague")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "leonid")
  (users (cons* (user-account
                  (name "kreyren")
                  (comment "Jacob Hrbek")
                  (group "users")
                  (home-directory "/home/kreyren")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service xfce-desktop-service-type)
            (service openssh-service-type)
            (service tor-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (targets (list "/dev/sda"))
      (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "1fe117ac-fe47-4eb3-a63b-b8a34dee4ea8"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "btrfs")
             (dependencies mapped-devices))
           %base-file-systems)))
