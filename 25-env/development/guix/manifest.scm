;;; Standard GNU Guix file defining packages for guix-shell environment

;; NOTE(Krey): For development of 'cargo-make' package, remove once merged
;(load "cargo-make.scm")

(use-modules
	(guix channels))

(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "f1bfd9f1948a5ff336d737c0614b9a30c2bb3097")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

(specifications->manifest
	;; BLOCKER(Krey): Cargo-make is not defined
 (list
  ;; CONTRIB-PKGS(Krey): Not packaged
  ;"rust-cargo-make"
  ;; CONTRIB-PKGS(Krey): Not packaged - https://issues.guix.gnu.org/51563
  ;"woodpeckerCI"
  "git"))
