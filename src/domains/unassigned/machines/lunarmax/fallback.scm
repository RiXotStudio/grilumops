;; TODO-LIBRE(Krey): Install libre BIOS

(use-modules (gnu))
(use-modules (nongnu packages linux)
	     (nongnu system linux-initrd))
(use-service-modules
  cups
  desktop
  networking
  ssh
  xorg)

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (locale "en_US.utf8")
  (timezone "Europe/Prague")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "lunarmax")
  (users (cons*
	   	(user-account
                  (name "raptor")
                  (comment "Raptor")
                  (group "users")
                  (home-directory "/home/raptor")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                (user-account
                  (name "sidonia")
                  (comment "Sidonia")
                  (group "users")
                  (home-directory "/home/sidonia")
                  (supplementary-groups
                    '("netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service xfce-desktop-service-type)
	    (service gnome-desktop-service-type)
            (service openssh-service-type)
            (service tor-service-type)
            (service cups-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (initrd-modules
    (append
      '("mmc_block" "sdhci_pci")
      %base-initrd-modules))
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "221b74ac-f047-4290-ad24-6f779f54f299"))
            (target "crypthome-sidonia")
            (type luks-device-mapping))
          (mapped-device
            (source
              (uuid "cc3664df-fe96-44e8-b23a-70ee58ff4d1c"))
            (target "crypthome-raptor")
            (type luks-device-mapping))
          (mapped-device
            (source
              (uuid "2d507235-a6ac-43e3-a4f6-f16bdfebd2fd"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "btrfs")
             (dependencies mapped-devices))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "C606-DE41" 'fat32))
             (type "vfat"))
           %base-file-systems)))
